<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>Day1</name>
    <message>
        <source>Day 1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Day2</name>
    <message>
        <source>Day 2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DayView</name>
    <message>
        <source>Go to streaming</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <source>Show Page 2</source>
        <translation>Zur Seite 2</translation>
    </message>
    <message>
        <source>UI Template</source>
        <translation>UI-Vorlage</translation>
    </message>
    <message>
        <source>Hello Sailors</source>
        <translation>Hallo Matrosen</translation>
    </message>
</context>
<context>
    <name>SecondPage</name>
    <message>
        <source>Nested Page</source>
        <translation>Unterseite</translation>
    </message>
    <message>
        <source>Item</source>
        <translation>Element</translation>
    </message>
</context>
</TS>
