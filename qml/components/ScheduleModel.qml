import QtQuick 2.0
import QtQuick.XmlListModel 2.0
import Sailfish.Silica 1.0

XmlListModel {
    source: "https://git.chaotikum.org/divoc/fahrplan/raw/master/fahrplan.xml"

    XmlRole { name: "title"; query: "title/string()" }
    XmlRole { name: "url"; query: "url/string()" }
    XmlRole { name: "description"; query: "description/string()" }
    XmlRole { name: "startTime"; query: "start/string()" }
}

