import QtQuick 2.0
import Sailfish.Silica 1.0


SilicaListView {
    property string pageTitle;

    anchors {
        fill: parent
        leftMargin: Theme.horizontalPageMargin
        rightMargin: Theme.horizontalPageMargin
    }
    contentHeight: height
    quickScroll: true
    quickScrollAnimating: true
    clip: true
    boundsBehavior: Flickable.DragOverBounds

    PullDownMenu {
        MenuItem {
            text: qsTr("Go to streaming")
            onClicked: Qt.openUrlExternally("https://media.ccc.de")
        }
    }

    header: PageHeader {
        title: pageTitle
    }
    headerPositioning: ListView.OverlayHeader

    delegate: ListItem {
        height: eventTitle.height + eventDescription.height + Theme.paddingMedium

        Row {
            id: eventRow
            width: parent.width

            Label {
                width: Theme.itemSizeLarge
                id: eventTime
                text: startTime
            }

            Column {
                width: parent.width - eventTime.width - Theme.paddingSmall
                spacing: Theme.paddingSmall

                Label {
                    width: parent.width
                    id: eventTitle
                    text: title
                    color: Theme.highlightColor
                    elide: "ElideRight"
                    MouseArea {
                        anchors.fill: parent
                        onClicked: Qt.openUrlExternally(url)
                    }
                }
                Label {
                    id: eventDescription
                    width: parent.width
                    text: description
                    elide: "ElideRight"
                    wrapMode: "Wrap"
                    height: visible ? Theme.itemSizeLarge : 0
                    visible: description !== ""
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if(eventDescription.elide === 1) { //ElideRight
                                eventDescription.elide = "ElideNone"
                                eventDescription.height = eventDescription.contentHeight
                            } else {
                                eventDescription.elide = "ElideRight"
                                eventDescription.height = Theme.itemSizeLarge
                            }
                        }
                    }
                }
            }
        }
    }
    VerticalScrollDecorator {}
}
