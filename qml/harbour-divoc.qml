import QtQuick 2.0
import Sailfish.Silica 1.0
import "pages"

ApplicationWindow
{
    Component.onCompleted: {
        var d = new Date();
        var page = "Day1"
        pageStack.push(Qt.resolvedUrl("./pages/Day1.qml"), {})
        pageStack.pushAttached(Qt.resolvedUrl("./pages/Day2.qml"), {})
        if(d.getDate() === 12) {
            pageStack.navigateForward()
        }
    }

    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations
}
