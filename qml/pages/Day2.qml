import QtQuick 2.0
import QtQuick.XmlListModel 2.0
import Sailfish.Silica 1.0
import "../components/"

Page {
    ScheduleModel {
        id: schedule
        query: "/schedule/day[2]/room/event"
    }

    DayView {
        model: schedule
        pageTitle: qsTr("Day 2");
    }
}

