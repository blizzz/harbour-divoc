import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    Image {
        width: parent.width + Theme.paddingLarge
        height: width
        x: -Theme.paddingLarge
        y: -Theme.paddingLarge
        horizontalAlignment: Image.AlignHCenter
        verticalAlignment: Image.AlignVCenter
        fillMode: Image.PreserveAspectCrop
        source: "../images/harbour-divoc.png"
    }

    CoverActionList {
        id: coverAction

        CoverAction {
            iconSource: "image://theme/icon-cover-play"
            onTriggered: Qt.openUrlExternally("https://media.ccc.de")
        }
    }
}
